/*global module:false, require:true*/
module.exports = function(grunt) {
	// Project configuration.
	var config = {
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			init_plugin: {
				options: {
					process: function (content, srcpath) {
						var pluginName = grunt.config('plugin.name');
						var nicePluginName = grunt.config('plugin.niceName');
						var namespace = grunt.config('plugin.namespace');
						var replacements = [
							{find: /\[\[year\]\]/g, replace: (new Date()).getFullYear() },
							{find: /\[\[author\]\]/g, replace: grunt.config( 'pkg.author' ) },
							{find: /\[\[description\]\]/g, replace: grunt.config( 'pkg.description' ) },
							{find: /SkeletonPlugin/g, replace: nicePluginName},
							{find: /skeleton-plugin/g, replace: pluginName},
							{find: /DBisso/g, replace: namespace},
						];

						for ( var i = 0; i < replacements.length; i++ ) {
							content = content.replace(replacements[i].find, replacements[i].replace);
						}

						return content;
					}
				},
				files: [
					{
						expand: true,
						cwd: 'lib/DBisso/Plugin/SkeletonPlugin/',
						src: '**',
						dest: './lib/<%= plugin.namespace %>/Plugin/<%= plugin.niceName %>/'
					},
					{
						src: 'package.json',
						dest: './package.json'
					},
					{
						src: 'plugin.sample.php',
						dest: './plugin.php'
					}
				]
			}
		},
		prompt: {
			init_plugin: {
        options: {
          questions: [
            {
              config: 'plugin.name',
              type: 'input',
              default: 'plugin-name',
              message: 'What do you want to name your _s theme (eg mythemename)?'
            },
            {
              config: 'plugin.niceName',
              type: 'input',
              default: 'PluginName',
              message: 'What is the nice name of your theme (PascalCased eg. MyThemeName)?'
            },
            {
              config: 'plugin.namespace',
              type: 'input',
              default: 'DBisso',
              message: 'What is the PHP namespace for your theme (PascalCased eg. DBisso)?'
            }
          ]
        }
      },
		}
	};

	// grunt.util._.extend(config, require('./config/grunt/init_plugin')(grunt));
	grunt.initConfig(config);

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-prompt');

  grunt.registerTask('init_plugin', [ 'prompt:init_plugin', 'copy:init_plugin' ]);
};