<?php
namespace DBisso\Plugin\RequestMusic;

/**
 * Class DBisso\Plugin\RequestMusic\Frontend
 */
class Frontend {
	static $hooker;
	static $form_errors = array();
	static $form_messages = array();
	static $form_values = array();
	const FORM_POST_KEY = 'dbisso-request-music';

	public function bootstrap( $hooker = null ) {
		if ( ! $hooker || ! method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );
	}

	public function get_request_form() {
		ob_start();
		$form_messages = self::$form_messages;
		$form_errors   = self::$form_errors;
		$form_values   = self::$form_values;

		include_once 'views/request-form-frontend.php';
		return ob_get_clean();
	}

	public function action_wp_enqueue_scripts() {
		wp_enqueue_script( 'jquery-ui-autocomplete' );
	}

	function shortcode_request_music_form( $atts, $content = '', $tag ) {
		$defaults = array(
			// '' => '',
		);

		$content = self::get_request_form();

		extract( shortcode_atts( $defaults, $atts ) );

		return do_shortcode( $content );
	}

	public function action_parse_request() {
		if ( isset( $_POST[self::FORM_POST_KEY] ) && !empty( $_POST[self::FORM_POST_KEY] ) ) {
			$values = array();

			foreach ( $_POST[self::FORM_POST_KEY] as $key => $value ) {
				$values[strip_tags( $key )] = self::sanitize_form_value( $value, $key );
			}

			if ( empty( $values['artist'] ) ) {
				self::$form_errors['artist'][] = __( 'You must choose an artist', 'dbisso-request-music' );
			}

			if ( empty( $values['track'] ) ) {
				self::$form_errors['track'][] = __( 'You forgot to choose a track', 'dbisso-request-music' );
			}

			if ( empty( $values['user'] ) ) {
				self::$form_errors['user'][] = __( 'Please let us know your name', 'dbisso-request-music' );
			}

			if ( ! empty( self::$form_errors ) ) {
				self::$form_values = $values;
				return;
			}

			$post = self::track_exists( $values );

			if ( ! $post ) {
				$post_array = self::get_new_music_request_args( $values );

				if ( ( $post_id = wp_insert_post( $post_array ) ) ) {
					add_post_meta( $post_id, 'dbisso_music_request_artist', $values['artist'] );
					add_post_meta( $post_id, 'dbisso_music_request_user', $values['user'] );

					error_log( 'create a post called ' . $values['track'] . ' by ' . $values['artist'] );
				} else {
					error_log( 'post not created' );
				}
			} else {
				error_log( 'add vote to ' . $post->post_title . ' for ' . $values['user'] );
				add_post_meta( $post->ID, 'dbisso_music_request_user', $values['user'] );
			}

			self::$form_messages[] = __( 'Thanks for your request. It has been added to the list.' );
		}
	}

	private function get_new_music_request_args( $values ) {
		return array(
			'post_type' => PostTypes::MUSIC_REQUEST,
			'post_title' => $values['track'],
			'post_status' => 'publish',
		);
	}

	private function track_exists( $values ) {
		$found = get_posts(
			array(
				'numberposts'		=> -1,
				'orderby'			=> 'post_date',
				'meta_key'			=> 'dbisso_music_request_artist',
				'meta_value'		=> $values['artist'],
				'post_type'			=> PostTypes::MUSIC_REQUEST,
				'post_title'        => $values['track'],
			)
		);

		$track = get_page_by_title( $values['track'], OBJECT, PostTypes::MUSIC_REQUEST );

		if ( $track && $track->dbisso_music_request_artist === $values['artist'] ) {
			return $track;
		}

		return false;
	}

	private function sanitize_form_value( $value, $key = '') {
		return strip_tags( $value );
	}
}