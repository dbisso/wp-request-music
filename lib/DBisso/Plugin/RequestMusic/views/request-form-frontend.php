<form action="" method="post" accept-charset="utf-8">
	<label for="dbisso-request-music-track"><?php _e( 'Your name', 'dbisso-request-music' ) ?></label>
	<input type="text" id="dbisso-request-music-user" name="dbisso-request-music[user]" value="<?php esc_attr_e( $form_values['user'] ) ?>" placeholder="Your name" />
	<?php if ( ! empty( $form_errors['user'] ) ) : ?>
		<?php foreach ( $form_errors['user'] as $error ) : ?>
			<span class="form-error"><?php echo wp_kses_post( $error ) ?></span>
		<?php endforeach ?>
	<?php endif ?>
	<br />

	<label for="dbisso-request-music-artist"><?php _e( 'Artist', 'dbisso-request-music' ) ?></label>
	<input type="text" id="dbisso-request-music-artist" name="dbisso-request-music[artist]" value="<?php esc_attr_e( $form_values['artist'] ) ?>" placeholder="Artist name" />
	<?php if ( ! empty( $form_errors['artist'] ) ) : ?>
		<?php foreach ( $form_errors['artist'] as $error ) : ?>
			<span class="form-error"><?php echo wp_kses_post( $error ) ?></span>
		<?php endforeach ?>
	<?php endif ?>
	<br />

	<label for="dbisso-request-music-track"><?php _e( 'Track', 'dbisso-request-music' ) ?></label>
	<input type="text" id="dbisso-request-music-track" name="dbisso-request-music[track]" value="<?php esc_attr_e( $form_values['track'] ) ?>" placeholder="Track name" />
	<?php if ( ! empty( $form_errors['track'] ) ) : ?>
		<?php foreach ( $form_errors['track'] as $error ) : ?>
			<span class="form-error"><?php echo wp_kses_post( $error ) ?></span>
		<?php endforeach ?>
	<?php endif ?>
	<br />


	<button type="submit">Request</button>
	<?php foreach ( $form_messages as $message ) : ?>
		<p class="message"><?php echo wp_kses_post( $message ) ?></p>
	<?php endforeach ?>

	<script type="text/javascript">
		jQuery(function($) {
			var artist = $("#dbisso-request-music-artist" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: "http://developer.echonest.com/api/v4/artist/suggest",
						dataType: "jsonp",
						data: {
							results: 10,
							api_key: "<?php echo ECHONEST_API_KEY ?>",
							format: "jsonp",
							name: request.term
						},
						success: function( data ) {
							response( $.map( data.response.artists, function(item) {
								return {
									label: item.name,
									value: item.name,
									id: item.id
								};
							}));
						}
					});
				},
				minLength: 3
			});

			$("#dbisso-request-music-track" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: "http://developer.echonest.com/api/v4/artist/songs",
						dataType: "jsonp",
						data: {
							results: 100,
							api_key: "<?php echo ECHONEST_API_KEY ?>",
							format: "jsonp",
							name: artist.val()
						},
						success: function( data ) {
							response( $.map( data.response.songs, function(item) {
								return {
									label: item.title,
									value: item.title,
									id: item.id
								};
							}));
						}
					});
				},
				minLength: 3
			});
		});
	</script>
</form>