<?php
namespace DBisso\Plugin\RequestMusic;
use DBisso\Plugin\RequestMusic\PostMeta;
use Echonest\Service\Echonest;

/**
 * Class DBisso\Plugin\RequestMusic\PostTypes
 */
class PostTypes {
	static $hooker;
	const MUSIC_REQUEST = 'dbisso_music_request';

	public static function bootstrap( $hooker = null ) {
		if ( ! $hooker || ! method_exists( $hooker, 'hook' ) )
			throw new \BadMethodCallException( 'Bad Hooking Class. Check that \DBisso\Util\Hooker is loaded.', 1 );

		self::$hooker = $hooker->hook( __CLASS__, $hooker->hook_prefix );

		add_filter( 'manage_' . self::MUSIC_REQUEST . '_posts_columns', array( __CLASS__, 'manage_request_edit_columns' ), 10, 99 );
		add_action( 'manage_' . self::MUSIC_REQUEST . '_posts_custom_column', array( __CLASS__, 'manage_edit_posts_columns_column' ), 10, 99 );
		add_filter( 'manage_edit-' . self::MUSIC_REQUEST . '_sortable_columns', array( __CLASS__, 'manage_request_edit_sortable_columns' ), 10, 99 );


	}

	public function action_init() {
		$args = array(
			'label' => __( 'Music Requests', 'dbisso-request-music' ),
			'labels' => array(
				'name' => __( 'Music Requests', 'dbisso-request-music' ),
				'singular_name' => __( 'Music Request', 'dbisso-request-music' ),
			),
			'public' => true,
		);

		register_post_type( self::MUSIC_REQUEST, $args );
	}

	public function manage_request_edit_columns( $columns ) {
		$columns['dbisso_music_request_artist'] = __( 'Artist', 'dbisso-request-music' );
		$columns['dbisso_music_request_user'] = __( 'Votes', 'dbisso-request-music' );
		unset( $columns['date'] );

		return $columns;
	}

	public function manage_edit_posts_columns_column( $column, $post_id ) {
		$post = get_post( $post_id );

		if ( self::MUSIC_REQUEST === $post->post_type ) {
			switch ( $column ) {
				case 'dbisso_music_request_artist':
					esc_html_e( $post->dbisso_music_request_artist );
					break;
				case 'dbisso_music_request_user':
					$users = get_post_meta( $post_id, 'dbisso_music_request_user' );
					echo count( $users );
					echo ' (' . strip_tags( implode( ', ', $users ) ) . ')';
					break;
				default:
					break;
			}
		}
	}

	public function manage_request_edit_sortable_columns( $columns ) {
		$columns['dbisso_music_request_artist'] = __( 'Artist', 'dbisso-request-music' );
		$columns['dbisso_music_request_user'] = __( 'Artist', 'dbisso-request-music' );
		return $columns;
	}
}