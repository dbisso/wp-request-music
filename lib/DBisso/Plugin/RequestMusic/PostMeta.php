<?php
namespace DBisso\Plugin\RequestMusic;

/**
 * Class DBisso\Plugin\RequestMusic\PostMeta
 */
class PostMeta {
	private $key;
	private $label;
	private $description;
	private $type;
	private $post_type;

	public function __construct( $args ) {
		$this->key = $args['key'];
		$this->label = $args['label'];
		$this->description = $args['description'];
		$this->type = $args['type'];
		$this->post_type = $args['post_type'];

		register_meta( 'post', $this->key );//, $sanitize_callback, $auth_callback = null );
	}
}